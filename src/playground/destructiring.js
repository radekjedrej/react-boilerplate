const person = {
  name: 'Radek',
  age: 30,
  location: {
    city: 'Kwidzyn',
    temp: 22
  }
}

console.log(`${person.name} is from ${person.location.city}`);

const { name: firstname = 'Anonymous', age } = person;
console.log(`${name} is ${age}`);

const { city, temp: temperature } = person.location;
console.log(`${name} is from ${city} where is ${temperature}deg`)


const address = ['1299 S Juniper Street', 'Philadelphia', 'Pensylvania', '19147'];

const [street, city2, state = 'New York', zip] = address;
console.log(`You are in ${street}, ${city2}`)


const item = ['Coffe (hot)', '$2.00', '$2.50', '$2.75'];
const [product, small, medium, large] = item;
console.log(`A medium ${product} cost ${medium}`);
