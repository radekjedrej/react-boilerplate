import React from 'react';
import { BrowserRouter, Route, Switch  } from 'react-router-dom';

import Header from '../components/Header';
import HomePage from '../components/HomePage';
import NotFoundPage from '../components/NotFoundPage';
import Portfolios from '../components/Portfolios';
import Portfolio from '../components/Portfolio';
import Contact from '../components/Contact';


const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/portfolios" component={Portfolios} />
          <Route path="/portfolios/:id" component={Portfolio} />
          <Route path="/contact" component={Contact} />
          <Route component={NotFoundPage} />
        </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;