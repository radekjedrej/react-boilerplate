import React from 'react';
import { BrowserRouter, Route, Switch, Link  } from 'react-router-dom';


const Portfolios = (props) => (
  <div>
    <h1>Portfolio</h1>
    <p>This is my dashboard component!</p>
    <ul>
      <li> 
        <Link to="/portfolios/1">Portoflio 1</Link>
      </li>
      <li>
        <Link to="/portfolios/2">Portoflio 2</Link>
      </li>
    </ul>
  </div>
);


export default Portfolios;